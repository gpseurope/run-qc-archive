# ----------------------------------------------------------------------------
# (c) 2017 Geodetic Observatory Pecny, http://www.pecny.cz (gnss@pecny.cz)
#     Research Institute of Geodesy, Topography and Cartography
#     Ondrejov 244, 251 65, Czech Republic
#
# Purpose: file download
#
# Created: 2017-02-18 Petr Bezdeka
# Updated: 2017-10-17 Jan Dousa
#
# ---------------------------------------------------------------------------
package Util_Get;

$ENV{'PERL_LWP_SSL_VERIFY_HOSTNAME'} = 0;

use strict;
use warnings;
use File::Path;
use File::Copy;
use File::Basename;
use LWP::UserAgent;
use IO::File;
# use Net::FTP;
# use Data::Dumper;

use lib dirname (__FILE__);
use Gps_Date;

use Exporter;

@Util_Get::ISA    = qw(Exporter);
@Util_Get::EXPORT = qw(get_FILE);

# Constructor
# ===========
sub new {
  my( $pkg  ) = shift;
  my( $verb ) = shift;
  my( $log  ) = shift;
   
  my $self =  {
    'verb' =>  $verb,
    'log'  =>  $log,
    'pkg'  =>  $pkg
  };

  bless $self, $pkg;
}

# Download BRDC
# =============
sub get_FILE()
{
  my $self     = shift;  my $log = $self->{'log'};
  my $urlFile  = shift; 
  my $locFile  = shift;  my $locDir = dirname("$locFile");

  if( ! -d "$locDir" and "$locDir" ne ""){
    $log->mesg($self->{'pkg'}, 1, "Directory created: ".$locDir);
    mkpath( $locDir, 0, 0774 );
  }

  my $remFile = $urlFile; 
     $remFile =~ s|ftp://\S+@||;

  $log->mesg($self->{'pkg'},1,"Download URL: $locFile"); # <-- $remFile");

  my $ua = LWP::UserAgent->new;  $ua->proxy(['http','ftp']);
    
  my $response = $ua->get("$urlFile", ':content_file' => $locFile);
    
  if( $response->is_success ){
    $log->mesg($self->{'pkg'},2,"Finished URL: $locFile"); # <-- $remFile");
    return $locFile;
  }

  # download failure !
  $log->warn($self->{'pkg'},0,"Download URL: $remFile failed: ".$response->status_line); # $! $
  return undef;
}

1;

__END__
