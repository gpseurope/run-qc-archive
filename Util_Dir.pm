# ----------------------------------------------------------------------------
# (c) 2017 Geodetic Observatory Pecny, http://www.pecny.cz (gnss@pecny.cz)
#     Research Institute of Geodesy, Topography and Cartography
#     Ondrejov 244, 251 65, Czech Republic
#
# Purpose: create/clean working/temporary directory
#
# Created: 2017-10-17 Jan Dousa
# Updated: 2017-10-24 Jan Dousa
#
# ---------------------------------------------------------------------------
package Util_Dir;

use strict;
use warnings;
use File::Path qw(mkpath remove_tree);
use File::Find;
use Gps_Date;

use Exporter;

@Util_Fil::ISA    = qw(Exporter);
@Util_Fil::EXPORT = qw(get_root create_temp clean_temp purge_temp);

# Constructor
# ===========
sub new {
  my( $pkg  ) = shift;
  my( $verb ) = shift;
  my( $log  ) = shift;
  my( $dir  ) = shift;
  my( $age  ) = shift;
    
  my $self =  {
    'verb' =>  $verb,      # verbosity
    'log'  =>  $log,       # log reference
    'dir'  =>  $dir,       # root dir path
    'tmp'  =>  "",         # temporary subdir (if used)
    'age'  =>  $age,       # [seconds]
    'pkg'  =>  $pkg
  };

  if( "$dir" =~ /^\s*#/ ){ return }
  
  if( ! -d "$dir" and $dir !~ /\%/  ){
    $log->mesg($pkg,0,"Directory created: $dir");
    mkpath( "$dir", 0, 0774 );
  }

  bless $self, $pkg;
}

# Get directory name
# ==================
sub get_root(){
  my $self = shift;
  my $dfmt = shift;              # gps_date format (to be substituted)
    
  my $dir  = $self->{'dir'};
  if( defined $dfmt && $dir =~ /\%/){ $dir = gps_date("$dfmt -o $dir") }
    
  if( ! -d "$dir" ){ mkpath("$dir") }

  return $dir;
}

# Get directory name
# ==================
sub get_temp(){
  my $self = shift;

  return $self->{'tmp'};
}

# Create directory
# ================
sub create_temp(){
  my $self = shift;  my $log = $self->{'log'};
  my $name = shift;  $name = "$$" if $name =~ /^\s*$/;
  my $dfmt = shift;              # gps_date format (to be substituted)
    
  my $dir  = $self->{'dir'};
  if( defined $dfmt && $dir =~ /\%/){ $dir = gps_date("$dfmt -o $dir") }
    
  $self->{'tmp'} = $dir."/$name";

  $log->mesg($self->{'pkg'}, 1, "Directory created: ".$self->{'tmp'});
  mkpath( $self->{'tmp'}, 0, 0774 );
}

# Clean temporary file
# ====================
sub clean_temp(){
  my $self = shift;  my $log = $self->{'log'};
  my $temp = $self->{'tmp'};

  $log->mesg($self->{'pkg'}, 0,"Cleaning temp: $temp/");

  remove_tree($temp, {verbose => 0, keep_root => 0});
}

# Purge temporary file
# ====================
sub purge_temp(){
  my $self = shift;  my $log = $self->{'log'};
  my $dfmt = shift;              # gps_date format (to be substituted)
    
  my $root = $self->{'dir'};
  if( defined $dfmt && $root =~ /\%/){ $root = gps_date("$dfmt -o $root") }
    
  $log->mesg($self->{'pkg'}, 0,"Cleaning temp old: $root");

  if( $self->{'age'} < 0 ){ return }

  my $now = time();
  my( @file_list, @dir_list );
  my( @find_dirs ) = ( "$root" );

  find( sub{ my $file = $File::Find::name;
             if( -f $file ){ push @file_list, $file; }
             if( -d $file ){ push @dir_list,  $file; }
  }, @find_dirs );

  if( scalar @dir_list  == 0 and
      scalar @file_list == 0 ){ return }

  for my $file ( @file_list )
  {
    my( @stats ) = stat( $file );
    if( $now - $stats[9] > $self->{'age'} ){
      $log->mesg($self->{'pkg'}, 2,"Removing file: $file");
      remove_tree($file, {verbose => 0, keep_root => 1});
    }
  }
 
  for my $dir ( @dir_list )
  {
    if( $dir =~ m|$root| ){ next }
    my( @stats ) = stat( $dir );
    if( $now - $stats[9] > $self->{'age'} ){
      $log->mesg($self->{'pkg'}, 1,"Removing dir: $dir");
      remove_tree($dir, {verbose => 0, keep_root => 1});
    }
  }
}
 
1;

__END__