# ----------------------------------------------------------------------------
# (c) 2017 Geodetic Observatory Pecny, http://www.pecny.cz (gnss@pecny.cz)
#     Research Institute of Geodesy, Topography and Cartography
#     Ondrejov 244, 251 65, Czech Republic
#
# Purpose: control RINEX files
#
# Created: 2017-02-18 Petr Bezdeka
# Updated: 2017-10-17 Jan Dousa
#
# ---------------------------------------------------------------------------
package Util_Rnx;

use strict;
use warnings;
use File::Copy;
use File::Basename;

use lib dirname (__FILE__);
use Util_Get;

use Exporter;
@Util_Rnx::ISA    = qw(Exporter);
@Util_Rnx::EXPORT = qw(copy_RNX get_RNX check_RNX);

# Constructor
# ===========
sub new {
  my( $pkg  ) = shift;
  my( $log  ) = shift;
   
  my $self =  {
    'log' =>  $log,
    'pkg' =>  $pkg
  };

  bless $self, $pkg;
}


# Copy RINEX
# ==========
sub copy_RNX
{
  my $self = shift;  my $log = $self->{'log'};
  my $src  = shift;
  my $dir  = shift;
  my $name = undef;
  my $file = undef;
      
  # Target path with name
  $name = basename $src;
  $file = $dir."/".$name;    

  if(copy($src,$dir)){
    $log->mesg($self->{'pkg'},2,"Copy file ".$src.": ok");
    return check_RNX($self, $file);
  }

  $log->warn($self->{'pkg'},1,"Copy file ".$src.": failed: $!");
  return "";
}


# Copy and uncompress RINEX
# ==========
sub copy_check_RNX
{ 
  my $self = shift;  my $log = $self->{'log'};
  my $src  = shift;
  my $dir  = shift;
  my $file = undef;
  my $type = undef;

  
  while(defined ($type = rnxGetType($src)) )
  {  
     # Target path with dir and basename
     $file = basename $src;
     $file = $dir."/".$file;
    
    if( $type eq "crz" )
    { 
      $file =~ s/d\.(z|Z|gz|gzip)$/o/g;
      $file =~ s/D\.(z|Z|gz|gzip)$/O/g;
      system("zcat $src | CRX2RNX - > $file");
      $log->mesg($self->{'pkg'},2, "Decompress CRZ Hatanaka file ".$src." to ".$file.": ok");
      return $file;
    } 
      
    elsif( $type eq "compress" )
    { 
      $file =~ s/\.(z|Z)$//g;
      $file =~ s/\.(gz|gzip)$//g;
      system("zcat $src > $file"); 
      $log->mesg($self->{'pkg'},2, "Decompress file ".$src." to ".$file.": ok");
      $src = $file;
    } 
      
    elsif( $type eq "hatanaka" )
    { 
      $file =~ s/[D]$/O/g;
      $file =~ s/[d]$/o/g;
      $file =~ s/CRX$/RNX/g;    
      $file =~ s/crx$/rnx/g;    
      system("cat $src | CRX2RNX - > $file");
      $log->mesg($self->{'pkg'},2,"Decompress CRX Hatanaka file ".$src." to ".$file.": ok");
      return $file;
    }
    
    elsif( $type eq "data" )
    { 
      return $file if($src eq $file);
      if(copy($src,$dir)){
        $log->mesg($self->{'pkg'},2,"Copy file ".$src." to ".$file.": ok");
        return $file;
      }
    }
    
    else{
      $log->warn($self->{'pkg'},1,"Undefined file extension: ".$src);
      return "";
    }
  }
  
  $log->warn($self->{'pkg'},1,"Unknown file extension: ".$src);

}


# Download RINEX
# ==============
sub get_RNX()
{
  my $self   = shift;  my $log = $self->{'log'};
  my $sess   = shift;
  my $url    = shift;
  my $name   = shift;
  my $tmpDir = shift;

  my $urlFile = $url.$name;

  my $inpf = gps_date( "-yd $sess -o $url/$name" );
  my $outf = gps_date( "-yd $sess -o $tmpDir"    );
      
  my $result = $self->get_FILE( $inpf, $outf );

  if( defined $result and -f "$result" ){ return $result; }

  return undef;
}


# Check RINEX
# ===========
sub check_RNX()
{
  my $self = shift;  my $log = $self->{'log'};
  my $file = shift;
  my $type = undef;

  while(defined ($type = rnxGetType($file)) )
  {

    if( $type eq "crz" )
    { 
      my $name = $file;
         $name =~ s/d\.(z|Z|gz|gzip)$/o/g;
         $name =~ s/D\.(z|Z|gz|gzip)$/O/g;
      system("zcat $file | CRX2RNX - > $name");
      $log->mesg($self->{'pkg'},2, "Decompress CRZ Hatanaka file ".$file.": ok"); # TODO: Check uncompress result
      $file = $name;
    }
      
    elsif( $type eq "compress" )
    {
      my $name = $file;
         $name =~ s/\.(z|Z)$//g;
         $name =~ s/\.(gz|gzip)$//g;
#        $name =~ s/\.(ZIP|zip)$//g;
      system("zcat $file > $name");
      $log->mesg($self->{'pkg'},2, "Decompress file ".$file.": ok"); # TODO: Check uncompress result
      $file = $name;
    }
      
    elsif( $type eq "hatanaka" )
    {
      my $name = $file; 
         $name =~ s/[D]$/O/g;
         $name =~ s/[d]$/o/g;
         $name =~ s/CRX$/RNX/g;
         $name =~ s/crx$/rnx/g;	
      system("cat $file | CRX2RNX - > $name");
      $log->mesg($self->{'pkg'},2,"Decompress Hatanaka file ".$file.": ok"); # TODO: Check Hatanaka result
      $file = $name;
    }

    elsif( $type eq "data" )
    {
      return $file;
    }
  
    else{
      $log->warn($self->{'pkg'},1,"Undefined file extension: ".$file);
     return "";
    }	    	    
  }

  $log->warn($self->{'pkg'},1,"Unknown file extension: ".$file);

  return "";
}


# Get RINEX file type
# ===================
sub rnxGetType(){
    
  my $file = lc( shift );

  if(    $file =~ /\.\d\d[do]\.z$/     ||
         $file =~ /\.\d\d[do]\.gzip$/  ||
         $file =~ /\.\d\d[do]\.gz$/  ){  return "crz";
  }elsif($file =~ /\.z$/     ||
#        $file =~ /\.zip$/   ||
         $file =~ /\.gzip$/  ||
         $file =~ /\.gz$/            ){  return "compress";
  }elsif($file =~ /\.crx$/           ){  return "hatanaka"; # rinex 3 Hatanaka
  }elsif($file =~ /\.rnx$/           ){  return "data";     # rinex 3
  }elsif($file =~ /\.\d\d[d]$/       ){  return "hatanaka"; # rinex 2 Hatanaka
  }elsif($file =~ /\.\d\d[o]$/       ){  return "data";     # rinex 2
  }elsif($file =~ /\.\d\d[pnglbf]$/  ){  return "data";     # rinex 2 navigation
  }else{                                 return undef;
  }
}
 
1;

__END__
