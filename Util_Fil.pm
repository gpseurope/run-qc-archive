#!/usr/bin/perl
# ----------------------------------------------------------------------------
# (c) 2018 G-Nut Software s.r.o. (gnss@gnutsoftware.cz)
# 
# (c) 2011-2017 Geodetic Observatory Pecny, http://www.pecny.cz (gnss@pecny.cz)
#     Research Institute of Geodesy, Topography and Cartography
#     Ondrejov 244, 251 65, Czech Republic
#
# Purpose: generate JSON file from data mask
#
# Created: 2017-02-18 Petr Bezdeka
# Updated: 2018-08-09 Jan Dousa
#
# ---------------------------------------------------------------------------
package Util_Fil;

use strict;
use warnings;
use File::Copy;
use File::Basename;
use IO::File;
use Data::Dumper;
use JSON;    # JSON::PP to be installed extra for canonical sorting, but not necessary

use Exporter;
use Gps_Date;

@Util_Fil::ISA    = qw(Exporter);
@Util_Fil::EXPORT = qw(gen_json FILETYPE);

# values in seconds
our $FILETYPE = {  "daily"  => { "len" => 86400, "smp" => 30 },
                   "hourly" => { "len" => 3600,  "smp" => 30 }
                };
  
# Constructor
# ===========
sub new {
  my( $pkg  ) = shift;
  my( $verb ) = shift;
  my( $log  ) = shift;
    
  my $self =  {
    'verb' =>  $verb,
    'log'  =>  $log,
    'pkg'  =>  $pkg
  };    
    
  bless $self, $pkg;
}

# Generate JSON (list of files), substitute DB-API input
# ======================================================
sub gen_json(){

    my $self     = shift;  my $log = $self->{"log"};
    my $date_ref = shift;  # reference date [%Y-%m-%d %H:%M:%S]  (if set requested)
    my $inp_file = shift;  # input files
    my $opt_meth = shift;  # file-list || file-mask
    
    my($ref_date, $ref_time, $beg_time);

    my $out_json = "";     # output json format
    my %JSON;              # temporary hash
    my @FILES;             # temporary array of files
    my @files;             # temporary array of files
    my @types;             # temporary array of types

    # input method
    if(    lc($opt_meth) eq 'file-mask' ){ push @files, glob( "$inp_file" ); }
    elsif( lc($opt_meth) eq 'file-list' ){ open(FIL, "<$inp_file");
                                           while(<FIL>){ chomp(); push( @files, $_ ) if -s "$_"; }
                                           close(FIL);
                                          }
    else{
      printf "gen_json: not proper method selected (file-list || file-mask)!\n";
      $out_json = encode_json( \%JSON ); return $out_json;
    }

#    printf "GEN_JSON: @files [$beg_time] $ref_date + $ref_time\n";

    foreach my $file ( @files ){
       if( ! -e "$file" ){ printf "Warning - cannot find file: [$file]\n"; last; }

       my $file_name  = basename $file;
       my $local_path =  dirname $file;
       my $site_name  = "";
       my $long_name  = 0;
       my $file_type  = "daily";
#       my $md5_sumZ   = "";
	
#       open( MD5, "zcat $file | md5sum |"); ( $md5_sumZ ) = <MD5> =~ /^(\S+)\s+[-]/; close(MD5);
#       if( $md5_sumZ eq "" ){ printf "Warning - cannot generate md5sum_compressed\n"; last; }
	
       # distinguish short file name vs long file name (site 4-char) vs (9-char)
       if( $file =~ /(\S\S\S\S\d\d\S\S\S)_\S_(\d\d\d\d)(\d\d\d)(\d\d)(\d\d)_\d\d(\S)_\d\d(\S)_\S(O).(crx|rnx).*/ ){
         $long_name = 1;
         $site_name = $1; # substr( $file_name, 0, 9 );
         $file_type = "hourly" if $6 eq "H";
         push(@types, $file_type) if !grep(/$file_type/,@types);
           
         $beg_time = gps_date(" -yd $2 $3 -hms $4:$5:00 -o %Y-%m-%d %H:%M:00");
       
       }elsif( $file =~ /(\S\S\S\S)(\d\d\d)(\S).(\d\d)[dDoO](.Z|.gz)/ ){
         $long_name = 0;
         $site_name = $1; # substr( $file_name, 0, 4 );
         $file_type = "hourly" if $3 =~ /[a-xA-X]/;
         push(@types, $file_type) if !grep(/$file_type/,@types);
           
         $beg_time = gps_date(" -yd $4 $2 -hid $3 -o %Y-%m-%d %H:00:00");
       
       }elsif( $file =~ /(\S\S\S\S)(\d\d\d)[a-xA-X].(\d\d)*/ ){
         $site_name = substr( $file_name, 0, 4 );
         $beg_time = gps_date(" -yd $3 $2 -o %Y-%m-%d 00:00:00");

       }else{
         $site_name = substr( $file_name, 0, 4 );           
       }

#       printf "date_ref = $date_ref   beg_time = $beg_time \n";
       if(    defined $date_ref ){ ($ref_date, $ref_time) = $date_ref =~ /(\d\d\d\d-\d\d-\d\d) (\d\d:\d\d:\d\d)/ }
       elsif( defined $beg_time ){ ($ref_date, $ref_time) = $beg_time =~ /(\d\d\d\d-\d\d-\d\d) (\d\d:\d\d:\d\d)/ }       
#       printf "ref_date = $ref_date   ref_time = $ref_time \n";

       my $hours = $FILETYPE->{$file_type}{'len'}/3600; # [hour]
       my $end_time = gps_date(" -ymd $ref_date -hms $ref_time -h +$hours -o %Y-%m-%d %H:%M:%S");
       my $sampling = $FILETYPE->{$file_type}{'smp'};   # [sec]

       $log->mesg($self->{'pkg'}, 1,"$file => $file_name [$file_type/$sampling] : $site_name : $beg_time ");
       my $tmp = { "station_name" => $site_name,
	                   "file_name" => $file_name,
	                  "local_path" => $local_path,
	                    "beg_time" => $beg_time,
	                    "end_time" => $end_time,
	                    "sampling" => $sampling,
#	                    "md5_sumZ" => $md5_sumZ,
                         "length" => $FILETYPE->{$file_type}{'len'},
                             "qc" => ($file_type eq "daily") ? "full" : "lite"
                 };

       push @FILES, $tmp;
    }
        
    if( scalar @types > 1 ){ 
      $log->error($self->{'pkg'},0,"Mixed file types (daily/hourly) ? Exit."); 
      return; 
    }
        
    $JSON{"files"} = \@FILES;
    $out_json = encode_json( \%JSON );
    return $out_json;
}

1;

__END__