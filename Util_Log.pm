# ----------------------------------------------------------------------------
# (c) 2017 Geodetic Observatory Pecny, http://www.pecny.cz (gnss@pecny.cz)
#     Research Institute of Geodesy, Topography and Cartography
#     Ondrejov 244, 251 65, Czech Republic
#
# Purpose: class for logging
#
# Created: 2014-10-01 Jan Dousa
# Updated: 2017-10-17 Jan Dousa
#
# ---------------------------------------------------------------------------
package Util_Log;

use strict;
use warnings;
use Exporter;

@Util_Log::ISA    = qw(Exporter);
@Util_Log::EXPORT = qw(mesg);

# Constructor
# ===========
sub new {
    my( $pkg  ) = shift;
    my( $verb ) = shift // 1;
    
    my $self =  { 'verb'   =>  $verb };
        
    bless $self, $pkg;
}

sub mesg {
    my $self = shift;
    my $name = shift;
    my $verb = shift;
    my $note = shift; $note =~ s|%|%%|g;

    printf "Mesg: $name [$verb]: $note \n" if $verb <= $self->{"verb"};
}


sub warn {
    my $self = shift;
    my $name = shift;
    my $verb = shift;
    my $note = shift; $note =~ s|%|%%|g;

    printf "Warn: $name [$verb]: $note \n" if $verb <= $self->{"verb"};
}

sub error {
    my $self = shift;
    my $name = shift;
    my $verb = shift;
    my $note = shift; $note =~ s|%|%%|g;

    printf "\n%s\n", "="x80;
    printf "*** Error: $name [$verb]: $note" if $verb <= $self->{"verb"};
    printf "\n%s\n", "="x80;
    exit(1);
}

sub getVerb {
    my $self = shift;

    return $self->{"verb"};
}

sub setVerb {
    my $self = shift;
    my $verb = shift;
    
    $self->{"verb"} = $verb;
}

 
1;
__END__