# ----------------------------------------------------------------------------
# (c) 2017 Geodetic Observatory Pecny, http://www.pecny.cz (gnss@pecny.cz)
#     Research Institute of Geodesy, Topography and Cartography
#     Ondrejov 244, 251 65, Czech Republic
#
# Purpose: configure Anubis
#
# Created: 2017-02-18 Petr Bezdeka
# Updated: 2017-10-17 Jan Dousa
#
# ---------------------------------------------------------------------------
package Util_Cfg;

use strict;
use warnings;
use XML::LibXML;
use File::Basename;

use lib dirname (__FILE__);
use Util_Get;
use Gps_Date;

use Exporter;
@Util_Cfg::ISA    = qw(Exporter);
@Util_Cfg::EXPORT = qw(default_cfg);

sub default_cfg
{
  my $rec  = shift;
  my $site = uc($rec->{"station_name"});
    
  # Create XML Config file
  my $doc = XML::LibXML::Document->new("1.0","UTF-8");
  $doc->setStandalone(1);
    
  # Create default elements                        # Create default structure
  my $elm_cfg = $doc->createElement("config");     $doc->setDocumentElement($elm_cfg);
  my $elm_gen = $doc->createElement("gen");        $elm_cfg->addChild($elm_gen);
  my $elm_beg = $doc->createElement("beg");        $elm_gen->addChild($elm_beg);
  my $elm_end = $doc->createElement("end");        $elm_gen->addChild($elm_end);
  my $elm_int = $doc->createElement("int");        $elm_gen->addChild($elm_int);
  my $elm_sys = $doc->createElement("sys");        $elm_gen->addChild($elm_sys);
  my $elm_rec = $doc->createElement("rec");        $elm_gen->addChild($elm_rec);
  my $elm_inp = $doc->createElement("inputs");     $elm_cfg->addChild($elm_inp);
  my $elm_out = $doc->createElement("outputs");    $elm_cfg->addChild($elm_out);
  my $elm_rxo = $doc->createElement("rinexo");     $elm_inp->addChild($elm_rxo);
  my $elm_rxn = $doc->createElement("rinexn");     $elm_inp->addChild($elm_rxn);
  my $elm_qc  = $doc->createElement("qc");         $elm_cfg->addChild($elm_qc);
  my $elm_xtr = $doc->createElement("xtr");        $elm_out->addChild($elm_xtr);
  my $elm_xml = $doc->createElement("xml");        $elm_out->addChild($elm_xml);
  my $elm_log = $doc->createElement("log");        $elm_out->addChild($elm_log);

  # Setup default attributes
  $elm_inp->setAttribute("chk_nav"    => "true");
  $elm_inp->setAttribute("chk_health" => "true");

  $elm_qc->setAttribute("sec_sum" => "3");
  $elm_qc->setAttribute("sec_hdr" => "1");
  $elm_qc->setAttribute("sec_est" => "1");
  $elm_qc->setAttribute("sec_obs" => "1");
  $elm_qc->setAttribute("sec_gap" => "0");
  $elm_qc->setAttribute("sec_bnd" => "0");
  $elm_qc->setAttribute("sec_pre" => "0");
  $elm_qc->setAttribute("sec_ele" => "0");
  $elm_qc->setAttribute("sec_mpx" => "0");
  $elm_qc->setAttribute("sec_snr" => "0");
  $elm_qc->setAttribute("sec_sat" => "0");
  $elm_qc->setAttribute("int_stp" => "1200");
  $elm_qc->setAttribute("int_psc" => "1800");
  $elm_qc->setAttribute("int_gap" => "600");
  $elm_qc->setAttribute("mpx_nep" => "20");
  $elm_qc->setAttribute("mpx_lim" => "3.0");
  $elm_out->setAttribute("verb"   => "0");
  
  # Create values - parameters of the function call
  $elm_beg->appendText($rec->{"beg_time"});
  $elm_end->appendText($rec->{"end_time"});
  $elm_int->appendText($rec->{"sampling"});

  # Create values - static -- BUT SHOULD BE ALSO AVAILABLE FROM DB-API!
# $elm_sys->appendText("GPS GLO GAL BDS SBS QZS");  # IRN
  $elm_sys->appendText("");
           
  # Create values - variable parameters !!!    
  $elm_rec->appendText( $site );
  $elm_rxo->appendText( $rec->{"rxo_path"} );
  $elm_rxn->appendText( $rec->{"nav_file"} ) if exists $rec->{"nav_file"};

  return $doc->toString;
}
 
1;

__END__