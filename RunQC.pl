#!/usr/bin/perl -wl
# ----------------------------------------------------------------------------
# (c) 2017-2018 Geodetic Observatory Pecny, http://www.pecny.cz (gnss@pecny.cz)
#     Research Institute of Geodesy, Topography and Cartography
#     Ondrejov 244, 251 65, Czech Republic
#
# Purpose: Script for generating GNSS QC metadata for GLASS using G-Nut/Anubis software
# Requires: libwww-perl, libjson-perl, libxml-perl, libxml-libxml-perl
#
# Created: 2017-02-18 Petr Bezdeka
# Updated: 2018-08-10 Jan Dousa
#
# ---------------------------------------------------------------------------
use strict;
use threads;
use warnings;
use 5.010;
use Data::Dumper;
use Getopt::Long;      # long get option command line
use Pod::Usage;        # help via command line
use File::Copy;
use File::Basename;
use File::Path qw( rmtree mkpath );

use lib dirname (__FILE__);
use JSON;
use Util_Pro;
use Util_Log;
use Util_Fil;
use Util_Get;
use Util_Api;
use Util_Rnx;
use Util_Nav;
use Util_Dir;
use Util_Cfg;
use Util_Run;
use Gps_Date;

our( $pgm ) = basename( $0 );

my( $json_inp ) = undef;                               # input json
my( $json_out ) = undef;                               # save json
my( $list_rxo ) = undef;                               # input file list RINEX
my( $mask_rxo ) = undef;                               # input file mask RINEX
my( $list_xml ) = undef;                               # input file list for existing QC-XML
my( $mask_xml ) = undef;                               # input file mask for existing QC-XML
my( $date_ref ) = undef;                               # reference date, e.g. gps_date("-t -o %Y-%m-%d %H:%M:%S")

my( $temp_dir ) = "/tmp";                              # temp directory (for decompressed RINEX files)
my( $brdc_dir ) = "BRD/%Y";                            # brdc directory
my( $brdc_fil ) = "";                                  # brdc local file to be used (preffered)
my( $brdc_upd ) = 0;                                   # get broadcasts

my( $conf_cmd ) = "";                                  # conf Anubis from the command line string 
my( $post_api ) = undef;                               # api url to post QC-XML to DB-API

my( $save_dir ) = "LOG/%Y/%j";                         # output directory
my( $save_log ) = 0;                                   # save Anubis LOG file
my( $save_cfg ) = 0;                                   # save Anubis CFG file
my( $save_xtr ) = 0;                                   # save Anubis XTR file
my( $save_xml ) = 1;                                   # save Anubis XML file

my( $pass_ftp ) = 0;                                   # enforce passive FTP

my( $time_lim ) = 99;                                  # [s] time limit for single Anubis
my( $fork_lim ) = 1;                                   # fork QC processes (run in parallel, # max forks)

my( $debug    ) = 0;                                   # debug
my( $help     ) = 0;                                   # help
my( $verb     ) = 1;                                   # level of verbosity

my( %FILES, %BRDC );

if(!@ARGV){ pod2usage({ -message => "\n$0: argument required", -exitval => 1, -verbose => 1 }) }

GetOptions( 'date_ref=s' => \$date_ref, # (string)  date format: "YYYY-DD-MM HH:MM:SS"
            'mask_rxo:s' => \$mask_rxo, # (string)  mask path (RXO)
            'list_rxo:s' => \$list_rxo, # (string)  file list (RXO)
            'json_inp:s' => \$json_inp, # (string)  file path (RXO)
            'json_out:s' => \$json_out, # (string)  file path (RXO)
            'mask_xml:s' => \$mask_xml, # (string)  mask path (XML)
            'list_xml:s' => \$list_xml, # (string)  file list (XML)
            'post_api:s' => \$post_api, # (string)  api url

            'temp_dir:s' => \$temp_dir, # (string)  temp directory path
            'brdc_dir:s' => \$brdc_dir, # (string)  brdc directory path
            'brdc_fil:s' => \$brdc_fil, # (string)  brdc file path
            'brdc_upd!'  => \$brdc_upd, # (boolean)

            'conf_cmd:s' => \$conf_cmd, # (string)  Anubis configuration from command-line

            'save_dir:s' => \$save_dir, # (string)  path supporting gps_date format

            'save_cfg!'  => \$save_cfg, # (boolean)
            'save_log!'  => \$save_log, # (boolean)
            'save_xtr!'  => \$save_xtr, # (boolean)
            'save_xml!'  => \$save_xml, # (boolean)
    
            'pass_ftp!'  => \$pass_ftp, # (boolean)
    
            'time_lim:i' => \$time_lim, # (integer) limit execution of single QC run
            'fork_lim:i' => \$fork_lim, # (integer) run QC in parallel mode (integer define max threads)

            'verb:i'     => \$verb,     # (integer)
            'debug'      => \$debug,    # (boolean)
            'help'       => \$help      # (boolean)
          )  or pod2usage(1);

my( $date, $time ) = ( '\d\d\d\d-\d\d-\d\d', '\d\d:\d\d:\d\d');
my( $log ) = new Util_Log($verb);
my( $rnx ) = new Util_Rnx($log);
my( $api ) = new Util_Api($verb,$log) if defined $post_api;
my( $run ) = new Util_Run($log,$debug,0,0,0,$api);

if( $help ){ pod2usage( -exitstatus => 1, -verbose => 99,
                        -sections   => "NAME|SYNOPSIS|DESCRIPTION|VERSION" )
};

if( defined $date_ref and $date_ref !~ /^$date\s*(|$time)/ ){
   $log->mesg($pgm,1,sprintf "%s", "invalid reference time [$date_ref], will be guessed from file name(s)");
   $date_ref = undef;
}

if( defined $mask_xml || defined $list_xml ){
  if(!defined $post_api ){ printf "\nError: can not post_api when QC-XML list not defined!\n\n"; pod2usage(-exitstatus => 1) };
  if( defined $list_rxo ){ printf "\nError: can not combine mask_xml and list_rxo options!\n\n"; pod2usage(-exitstatus => 1) };
  if( defined $mask_rxo ){ printf "\nError: can not combine mask_xml and mask_rxo options!\n\n"; pod2usage(-exitstatus => 1) };
  if( defined $json_inp ){ printf "\nError: can not combine mask_xml and json_inp options!\n\n"; pod2usage(-exitstatus => 1) };

  $api->post_QCXML($post_api, $mask_xml,"file-mask") if defined $api && defined $mask_xml;
  $api->post_QCXML($post_api, $list_xml,"file-list") if defined $api && defined $list_xml;
  exit 0;
}

my( $anubis, $crz2rnx );
open(ANUBIS,  "which anubis             |" ); ($anubis ) =  <ANUBIS> =~ m|^(\S+)\n|; close(ANUBIS);
open(CRZ2RNX, "which CRX2RNX            |" ); ($crz2rnx) = <CRZ2RNX> =~ m|^(\S+)\n|; close(CRZ2RNX);

if( ! defined $anubis  && ! -x $anubis ){ $log->error($pgm,1,"Software G-Nut/Anubis for quality check not found.") }
if( ! defined $crz2rnx && ! -x $crz2rnx){ $log->error($pgm,1,"Software CRZ2RNX for Hatanaka decompression not found.") }

if( $save_cfg eq 0 and $save_log eq 0 and $save_xtr eq 0 and $save_xml eq 0 ){ $save_dir = "" }

$log->mesg($pgm,1,sprintf "%s", "="x40);
$log->mesg($pgm,1,sprintf "SET list_rxo = %s", $list_rxo) if defined $list_rxo;
$log->mesg($pgm,1,sprintf "SET mask_rxo = %s", $mask_rxo) if defined $mask_rxo;
$log->mesg($pgm,1,sprintf "SET list_xml = %s", $list_xml) if defined $list_xml;
$log->mesg($pgm,1,sprintf "SET mask_xml = %s", $mask_xml) if defined $mask_xml;
$log->mesg($pgm,1,sprintf "SET json_inp = %s", $json_inp) if defined $json_inp;
$log->mesg($pgm,1,sprintf "SET json_out = %s", $json_out) if defined $json_out;
$log->mesg($pgm,1,sprintf "SET date_ref = %s", $date_ref) if defined $date_ref;
$log->mesg($pgm,1,sprintf "SET temp_dir = %s", $temp_dir);
$log->mesg($pgm,1,sprintf "SET brdc_dir = %s", $brdc_dir);
$log->mesg($pgm,1,sprintf "SET brdc_fil = %s", $brdc_fil);
$log->mesg($pgm,1,sprintf "SET brdc_upd = %s", $brdc_upd);
$log->mesg($pgm,1,sprintf "SET pass_ftp = %s", $pass_ftp);
$log->mesg($pgm,1,sprintf "SET fork_lim = %s", $fork_lim);
$log->mesg($pgm,1,sprintf "SET time_lim = %s", $time_lim);
$log->mesg($pgm,1,sprintf "SET save_dir = %s", $save_dir);
$log->mesg($pgm,1,sprintf "SET save_cfg = %s", $save_cfg);
$log->mesg($pgm,1,sprintf "SET save_log = %s", $save_log);
$log->mesg($pgm,1,sprintf "SET save_xtr = %s", $save_xtr);
$log->mesg($pgm,1,sprintf "SET save_xml = %s", $save_xml);
$log->mesg($pgm,1,sprintf "SET verb     = %s", $verb    );
$log->mesg($pgm,1,sprintf "SET debug    = %s", $debug   );
$log->mesg($pgm,1,sprintf "PGM anubis   = %s", $anubis  );
$log->mesg($pgm,1,sprintf "PGM crz2rnx  = %s", $crz2rnx );
$log->mesg($pgm,2,sprintf "SET conf_cmd = %s", $conf_cmd || '""');
$log->mesg($pgm,1,sprintf "%s", "="x40);

$log->mesg($pgm,0,"RunQC process started");

# Handle temporary directories
# ============================
my $logDir = new Util_Dir($verb, $log, "${save_dir}", -1);    # can include gps_date format
my $tmpDir = new Util_Dir($verb, $log, "${temp_dir}/qc/", 60*60*24);
   $tmpDir->create_temp( gps_date("-t -o %y%j.$$") );

# Clean past temporary dir(s)
# ===========================
if( -e $tmpDir->get_temp() ){
  $tmpDir->purge_temp() unless $debug;
}

# Prepare JSON input
# ==================
my( $strJson1, $strJson2, $strJson3 ) = ("","","");

if( defined $mask_rxo ){ # JSON from file-mask argument (list in local repository)
   my $lst = new Util_Fil($verb,$log);
   $strJson1 = $lst->gen_json($date_ref,$mask_rxo,"file-mask");
}

if( defined $list_rxo ){ # JSON from file-mask argument (list in local repository)
   my $lst = new Util_Fil($verb,$log);
   $strJson2 = $lst->gen_json($date_ref,$list_rxo,"file-list");
}

if( defined $json_inp ){ # JSON from argument
   open(FILE, "$json_inp");
   while (my $line = <FILE> ){ $strJson3 .= $line; };
   close(FILE);
}

#     # JSON from DB-API call (unchecked rinex files)
# {    
#    # ======================================================
#    # TODO TODO TODO TODO TODO TODO TODO TODO TODO TODO TODO
#    # ======================================================
#    $log->warn($pgm,3,"Input JSON from DB-API not yet implemented"); exit(1);
# }

# Cconvert JSON string to hash
# ===========================
%FILES = %{decode_json($strJson1)} if "$strJson1" ne "";
%FILES = %{decode_json($strJson2)} if "$strJson2" ne "";
%FILES = %{decode_json($strJson3)} if "$strJson3" ne "";

if( keys %FILES == 0 ){ $log->warn($pgm,0,"No files found!"); exit(1); }

# Save json if requested
# ======================
if( defined $json_out ){
# open( JSON, ">$json_out"); printf JSON $strJson1; printf JSON $strJson2; printf JSON $strJson3; close( JSON );
  open( JSON, ">$json_out"); printf JSON encode_json( \%FILES ); close( JSON );
}

# Continue if #rinex_files > 0
# ============================
my $count_files = scalar @{$FILES{"files"}};

$log->mesg($pgm,1,"# files to be processed: ".$count_files);

if( $pass_ftp > 0 ){
  $ENV{'FTP_PASSIVE'} = "true";
  $log->mesg($pgm,2,"passive FTP enforced (FTP_PASSIVE=true)");
} 

# Copy (or download) and check RINEX files and supporting BRDC files
# ==================================================================
for( my $n=0; $n<$count_files; $n++ )
{    
  my $rec = $FILES{"files"}[$n];
    
  my $time_fmt = "%y%j%i";
     $time_fmt = "%y%j%I" if $rec->{"station_name"} eq uc($rec->{"station_name"});

  my( $begD, $begT ) = split / /, $rec->{"beg_time"};
  my( $endD, $endT ) = split / /, $rec->{"end_time"};
   
  # Distinguish daily files
  if( $rec->{"length"} == $Util_Fil::FILETYPE->{"daily"}{"len"} ){
    ( $begT, $endT, $time_fmt ) = ( "00:00:00","00:00:00","%Y%j0" );
  }
    
  # Define observation local path (keeping the same file name)
  my $locFile = $rec->{"local_path"}."/".$rec->{"file_name"};

  # Reference session
  my $sess = gps_date("-ymd $begD -hms $begT -o $time_fmt");
    
  # Copy observation file
  $rec->{"rxo_path"} = $rnx->copy_check_RNX($locFile, $tmpDir->get_temp());
      
  # Download necessary broadcast file(s) to BRDC archive - if requested
  if( $rec->{"qc"} eq "full" )
  {
    if( !exists $BRDC{$sess} or !-s $BRDC{$sess}[0] or $brdc_upd )
    {
      if( $brdc_fil !~ /^\s*$/ ){
        my $nav_fil  = gps_date("-ymd $begD -hms $begT -o ${brdc_fil}");
#       my $nav_tmp  = $rnx->copy_RNX($nav_fil, $tmpDir->get_temp());
        if( -s ${nav_fil} ){
          $BRDC{$sess} = [ $nav_fil ];
          $log->mesg($pgm,1,"Local NAV file used: $nav_fil");
        }
      }

      if( !exists $BRDC{$sess} or !-s $BRDC{$sess}[0] ){
        my $nav      = new Util_Nav( $verb, $log, $brdc_dir );
        my( @files ) = $nav->get_BRDC( $sess, $brdc_upd );
        $BRDC{$sess} = \@files if scalar @files > 0;
      }
    }
  }

  # Copy necessary broadcast file(s) to TEMP dir
  foreach my $brdfile ( @{$BRDC{$sess}} ){ 
    if ( -f $brdfile ){ $rec->{"nav_file"} .= " ".$rnx->copy_check_RNX( $brdfile, $tmpDir->get_temp() ) }
  }

  if( defined $rec->{"nav_file"} ){ $log->mesg($pgm,2,"NAV file:".$rec->{"nav_file"}); }
  elsif($rec->{"qc"} ne "full"   ){ $log->mesg($pgm,2,"NAV file skipped for other than daily files"); }
  else{                             $log->warn($pgm,0,"NAV file not found !"); }

  $log->mesg($pgm,1,"Prepare file: ".$rec->{'file_name'});
}

# Create configuration file & run Anubis
# ======================================
for( my $n=0; $n<$count_files; $n++ )
{	
  my $rec = $FILES{"files"}[$n];

  my $time_fmt = "%y%j%i"; 
     $time_fmt = "%y%j%I" if $rec->{"station_name"} eq uc($rec->{"station_name"});

  my( $begD, $begT ) = split / /, $rec->{"beg_time"};
  my( $endD, $endT ) = split / /, $rec->{"end_time"};

  # Distinguish daily files
  if( $rec->{"length"} == $Util_Fil::FILETYPE->{"daily"}{"len"} ){
    ( $begT, $endT, $time_fmt ) = ( "00:00:00","00:00:00","%Y%j0" );
  }

  my $ideName = $rec->{"station_name"}.gps_date("-ymd $begD -hms $begT -o $time_fmt");
  my $outName = $logDir->get_root("-ymd $begD -hms $begT")."/".$ideName;
  my $tmpName = $tmpDir->get_temp(                       ).'/'.$ideName;

  my $cfgFile = $tmpName.".cfg";   $cfgFile = $outName.".cfg"  if $save_cfg;
  my $logFile = $tmpName.".log";   $logFile = $outName.".log"  if $save_log;
  my $xmlFile = $tmpName.".xml";   $xmlFile = $outName.".xml"  if $save_xml;
  my $xtrFile = $tmpName.".xtr";   $xtrFile = $outName.".xtr"  if $save_xtr;

  my $anub_out  = "";
     $anub_out .= "           -Z $cfgFile"  if $save_cfg;
     $anub_out .= " :outputs:log $logFile"; # always save log (even for temporary)
     $anub_out .= " :outputs:xtr $xtrFile"  if $save_xtr;
     $anub_out .= " :outputs:xml $xmlFile"; # always generate QC_XML!

  # Check files exist TODO: Err
  if( !(-f $rec->{"rxo_path"}) ){
    $log->warn($pgm,1,"Observation file not created!");
  }
    
  # Create CFG XML file (rinex by rinex)
  my $strCfg = default_cfg( $rec );
  if( $strCfg ne "" )
  {	
    open(FILE, ">$cfgFile");
    print FILE $strCfg;
    close(FILE);
    $log->mesg($pgm,2,"Config file created");
  }else{
    $log->warn($pgm,1,"Config file not created!");
  }       

  # re-configure non-daily data (hourly, high-rate, etc)
  my $cnrt  = "";
     $cnrt .= " :qc:sec_sum=1 :qc:sec_hdr=1 :qc:sec_obs=1 :qc:sec_pre=1 :qc:sec_gap=1" if defined $rec->{'qc'} && $rec->{'qc'} ne "full";
     $cnrt .= " :qc:sec_ele=0 :qc:sec_bnd=0 :qc:sec_snr=0 :qc:sec_est=0 :qc:sec_sat=0" if defined $rec->{'qc'} && $rec->{'qc'} ne "full";

  my $command = "$anubis -x $cfgFile $conf_cmd $anub_out $cnrt";
  my $options = { "conf_xml" => $cfgFile,
                  "file_xml" => $xmlFile, 
                  "file_xtr" => $xtrFile,
                  "site_nam" => $rec->{"station_name"},
                  "date_ref" => $date_ref,
                  "post_api" => $post_api,
                       "rec" => $rec,
                        "qc" => $rec->{"qc"}
  };

  # Run Anubis
  if(! $debug ){ 
    
    # parallel or sequence mode
    if( $fork_lim > 0 ){
        
      # join if fork clusters filled
      if( $n > 0 && $n%$fork_lim == 0 ){
        $log->mesg($pgm,1,"Max fork reached [$fork_lim], wait for joining");
        $_->join for threads->list;
      }

      threads->create( sub{ $run->run_Anubis($time_lim, $verb, $options, $command) } );
    }else{                  $run->run_Anubis($time_lim, $verb, $options, $command);}

  }else{
    $log->mesg($pgm,0,"$command .. NOT STARTED (debug mode)!");
  }
}

# Wait for all sub-processes
# ==========================
if( $fork_lim ){ $_->join for threads->list; }

# Delete temporary dir(s)
# =======================
if( -e $tmpDir->get_temp() ){
  $tmpDir->clean_temp() unless $debug;
  $tmpDir->purge_temp() unless $debug;
}

$log->mesg($pgm,0,"RunQC process finished");


# ======================================
__END__

=head1 NAME

 RunQC.pl - generates QC metadata for GNSS files in local repository.
          - communicates with DB-API to cross-validated metadata and populate DB.

=head1 SYNOPSIS

 RunQC.pl [options]
  
 Options:

     --mask_rxo  string       .. mask for files in repository                     [MANDATORY]
     --list_rxo  string       .. list for files in repository                     [ALTERNATIVE]

     --date_ref  string       .. file reference time: "YYYY-MM-DD HH:MM:SS"       [OPTIONAL]

     --mask_xml  string       .. mask of existing QC-XML files                    [OPTIONAL] post_api is only called
     --list_xml  string       .. list of existing QC-XML files                    [OPTIONAL] post_api is only called

     --json_inp  string       .. input JSON file                                  [OPTIONAL]
     --json_out  string       .. store JSON file                                  [OPTIONAL]

     --post_api  string       .. if url, post QC_XML to DB-API Web server         [ACTIVATE the POST function, default:NONE]

     --brdc_dir  string       .. local path to brdc local archive                 [default:BRD/%Y]
     --brdc_fil  string       .. local path+name of brdc to be used prefferably   [default:""]
     --brdc_upd               .. update brdc files in local archive               [default:true]
     --pass_ftp               .. enforce passive ftp for brdc download            [default:false, i.e. implicite mode]

     --conf_cmd  string       .. configure Anubis from the command line string    [default:""]

     --time_lim  integer      .. time limit for single QC run [sec]               [default:99]
     --fork_lim  ingeger      .. run QC in parallel (int define max forks)        [default:0]
 
     --save_dir  string       .. output directory (may include gps_date format)   [default:LOG/%Y/%j]
     --save_log               .. save Anubis LOG file                             [default:false]
     --save_cfg               .. save Anubis CFG file                             [default:false]
     --save_xtr               .. save Anubis XTR file                             [default:false]
     --save_xml               .. save Anubis XML file                             [default:true]

     --temp_dir  string       .. temporary directory for uncompressed RINEX files [default:/tmp]
     
     --verb      integer      .. level of verbosity                               [default:1]
     --help                   .. help message                                     [default:false] 
     --debug                  .. debug mode                                       [default:false]

=head1 DESCRIPTION
    
B<This program> generates QC metadata for GNSS files from a local repository
and populates them to the QC database (via DB-API).

The program requires that the enviroment variable PATH includes location
to the G-Nut/Anubis and CRX2RNX software.

=cut
