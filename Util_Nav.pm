# ----------------------------------------------------------------------------
# (c) 2017 Geodetic Observatory Pecny, http://www.pecny.cz (gnss@pecny.cz)
#     Research Institute of Geodesy, Topography and Cartography
#     Ondrejov 244, 251 65, Czech Republic
#
# Purpose: get broadcast ephemeris
#
# Created: 2017-02-18 Petr Bezdeka
# Updated: 2017-10-17 Jan Dousa
#
# ---------------------------------------------------------------------------
package Util_Nav;

use strict;
use warnings;
use File::Basename;
use File::Path qw( rmtree mkpath );

use lib dirname (__FILE__);
use Util_Get;
use Gps_Date;

use Exporter;
@Util_Nav::ISA    = qw(Exporter);
@Util_Nav::EXPORT = qw(get_BRDC);

my( $RX2 ) = { "GNS" => 'p', "GPS" => 'n', "GLO" => 'g', "GAL" => 'l', "SBS" => 'b' };
my( $RX3 ) = { "GNS" => 'M', "GPS" => 'G', "GLO" => 'R', "GAL" => 'E', "SBS" => 'S',
                                           "BDS" => 'C', "QZS" => 'J', "IRN" => 'I' };
my( $FTP ) = {
#      'GOP_V3' => { 'url'   => 'ftp://anonymous:gnss.qc@ftp.pecny.cz',
      'GOP_V3' => { 'url'   => 'http://www.pecny.cz/ftp',
                    'file'  => 'LDC/orbits_brd/gop3/%Y/BRDC00GOP_R_%Y%j0000_01D_<rx3type>N.rnx.gz',     # Mixed GNSS
                    'name'  => 'BRDC00GOP_R_%Y%j0000_01D_<rx3type>N.rnx.gz',
                    'type'  => 'GNS' },

      'GOP_V2' => { 'url'   => 'ftp://anonymous:gnss.qc@ftp.pecny.cz',
                    'file'  => 'LDC/orbits_brd/gop2/%Y/brdc%j0.%y<rx2type>.gz',     # Mixed GNSS
                    'name'  => 'brdg%j0.%y<rx2type>.gz',
                    'type'  => 'GPS GLO' },

      'TUM_V3' => { 'url'   => 'ftp://anonymous:gnss.qc@cddis.gsfc.nasa.gov',
                    'file'  => 'gnss/data/campaign/mgex/daily/rinex3/%Y/brdm/brdm%j0.%y<rx2type>.Z',     # Mixed GNSS
	                 'name'  => 'brdm%j0.%y<rx2type>.gz',
	                 'type'  => 'GNS' },
    
      'BKG_V3' => { 'url'   => 'ftp://anonymous:gnss.qc@igs.bkg.bund.de',
                    'file'  => 'IGS/BRDC/%Y/%j/BRDC00WRD_R_%Y%j0000_01D_<rx3type>N.rnx.gz',         # Mixed GNSS
                    'name'  => 'BRDC00BKG_R_%Y%j0000_01D_<rx3type>N.rnx.gz',
                    'type'  => 'GNS' },

      'BKG_V2' => { 'url'   => 'ftp://anonymous:gnss.qc@igs.bkg.bund.de',
	                 'file'  => 'IGS/BRDC/%Y/%j/brdc%j0.%y<rx2type>.Z',
                    'name'  => 'bkg_%j0.%y<rx2type>.gz',
                    'type'  => 'GPS GLO' },

      'IGS_V3' => { 'url'   => 'ftp://anonymous:gnss.qc@cddis.gsfc.nasa.gov',
                    'file'  => 'gps/data/daily/%Y/brdc/BRDC00IGS_R_%Y%j0000_01D_MN.rnx.gz', # Mixed GNSS
                    'name'  => 'BRDC00IGS_R_%Y%j0000_01D_<rx3type>N.rnx.gz',
                    'type'  => 'GNS' },

      'IGS_V2' => { 'url'   => 'anonymous:gnss.qc@cddis.gsfc.nasa.gov',
                    'file'  => 'gps/data/daily/%Y/brdc/brdc%j0.%y<rx2type>.Z',
                    'name'  => 'igs_%j0.%y<rx2type>.gz',
                    'type'  => 'GPS GLO' },
   };
#  'HOURLY_V3' => { 'url'   => 'ftp://anonymous:gnss.qc@cddis.gsfc.nasa.gov' =>
#                   'file'  => 'gps/data/hourly/%Y/%j/??/?????????_R_%Y%j??00_01H_MN.rnx.gz' }  # ?????


# Constructor
# ===========
sub new
{
  my( $pkg  ) = shift;
  my( $verb ) = shift;
  my( $log  ) = shift;
  my( $dir  ) = shift;
    
  my $self = {
    'verb' =>  $verb,
    'log'  =>  $log,
    'dir'  =>  $dir,
    'pkg'  =>  $pkg
  };

  if( "$dir" =~ /^\s*#/ ){ return }
  
  if( ! -d "$dir" and $dir !~ /\%/  ){
    $log->mesg($pkg,0,"Directory created: $dir");
    mkpath( "$dir", 0, 0774 );
  }

  bless $self, $pkg;
}

# Download BRDC
# =============
sub get_BRDC()
{
  my $self     = shift;  my $log = $self->{'log'};
  my $sess     = shift;
  my $update   = shift || 0;           # 1: always request BRDC update (in archive)
    
  my @result = ();  
  foreach my $id ( "GOP_V3", "TUM_V3", "IGS_V3", "BKG_V3",     # Priority for RINEX3 !
                   "GOP_V2",           "IGS_V2", "BKG_V2"      # Use for historical reasons
                 )
  {    
    if( ! exists $FTP->{$id} ){
      $log->warn($self->{'pkg'},1,"FTP: $id not defined");
      next;
    }

    foreach my $gnss ( split '\s+', $FTP->{$id}->{'type'} )
    {	
      my $inpf = gps_date( "-yd $sess -o $FTP->{$id}->{'url'}/$FTP->{$id}->{'file'}" );
      my $outf = gps_date( "-yd $sess -o       $self->{'dir'}/$FTP->{$id}->{'name'}" );
      
      $inpf =~ s|<rx2type>|$RX2->{$gnss}|g if exists $RX2->{$gnss};
      $inpf =~ s|<rx3type>|$RX3->{$gnss}|g if exists $RX3->{$gnss};
      $outf =~ s|<rx2type>|$RX2->{$gnss}|g if exists $RX2->{$gnss};
      $outf =~ s|<rx3type>|$RX3->{$gnss}|g if exists $RX3->{$gnss};

      if( -f $outf and ! $update ){
        $log->warn($self->{'pkg'},3,"Not downloaded as found in archive [$outf]");
        push( @result, $outf );
      }else{
        my $file = $self->get_FILE( $inpf, $outf );
        push( @result, $file ) if defined $file and -f "$file";
      }
    }

    if( scalar @result > 0 ){ return @result; }
    else{ $log->warn($self->{'pkg'},3,"FTP: $id not successful"); }
  }
  return @result;
}

1;

__END__